import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { ProductsController } from './products/products.controller';
import { AppService } from './app.service';
import { ProductsModule } from './products/products.module';
import { ProductService } from './products/products.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { Products } from './entity/products.entity';
import { InventoryModule } from './inventory/inventory.module';

@Module({
  imports: [InventoryModule, ProductsModule],
  controllers: [AppController],
  providers: [AppService],
})

export class AppModule {}
