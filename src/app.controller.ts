import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getInteger(): JSON {
    const stringHelloWorld = this.appService.getHello();
    const otherOb = this.appService.getInteger();
    
    const finalObj = {
      stringVar: stringHelloWorld,
      otherObj: otherOb
    }
    
    return JSON.parse(JSON.stringify(finalObj));
  }

  @Get()
  getHello(): JSON {
    const stringHelloWorld = this.appService.getHello();
    const object = {
      string: stringHelloWorld
    };

    return JSON.parse(JSON.stringify(object));
  }
}
