import { Entity, Column, PrimaryGeneratedColumn, OneToOne, ManyToOne, JoinColumn, TableForeignKey } from 'typeorm';
import { Products } from './products.entity';

@Entity()
export class Inventory {
    @PrimaryGeneratedColumn()
    id: number;

    @OneToOne(() => Products)
    @JoinColumn({ name: 'product_id'})
    product_id: number

    @Column()
    stocks: number;

    @Column()
    status: string;
}