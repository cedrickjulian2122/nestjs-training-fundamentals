import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Sales } from './sales.entity';

@Entity()
export class Products {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    description: string;

    @Column()
    price: number;

    @OneToMany(() => Sales, sales => sales.products)
    products: Sales[]
}