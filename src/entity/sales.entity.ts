import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm'
import { Products } from './products.entity';

@Entity()
export class Sales {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    quantity: string;

    @Column()
    price: number;

    @ManyToOne(() => Products, products => products.products)
    products: Products[]
}