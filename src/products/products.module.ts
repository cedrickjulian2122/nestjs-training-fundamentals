import { Module } from '@nestjs/common';
import { ProductsController } from "./products.controller";
import { ProductService } from './products.service';
import { DatabaseModule } from 'src/database/database.module';
import { productProviders } from './products.providers';
import { inventoryProviders } from 'src/inventory/inventory.providers';

@Module({
    imports: [DatabaseModule],
    controllers: [ProductsController],
    providers: [...inventoryProviders, ...productProviders, ProductService],
})

export class ProductsModule {}