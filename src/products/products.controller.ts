import { Controller, Post, Body, Get, Param, Delete, Patch } from "@nestjs/common";
import { ProductService } from './products.service';

@Controller('products')
export class ProductsController {
    
    constructor(private readonly productsService: ProductService) {}

    @Post()
    async addProduct(
        @Body('title') prodTitle: string,
        @Body('description') prodDesc: string,
        @Body('price') prodPrice: number,
        @Body('stocks') inventoryStock : number
    ): Promise<{}> {
        const result = await this.productsService.inserProduct(prodTitle, prodDesc, prodPrice, inventoryStock);

        const response = {
            message: "Successfuly created product",
            data: result
        };

        return response;
    }

    @Get()
    async getProducts() : Promise<{}> {
        const data = await this.productsService.getProducts();
        
        const response = {
            message: "Successfuly retrieved all products",
            data: data
        }

        return response;
    }

    @Get(':id')
    async searchProduct(
        @Param('id') id : number
    ) : Promise<{}> {
        const product = await this.productsService.getSpecificProduct(id);
        const response = {
            message: "Search Result",
            data: product
        }

        return response;
    }

    @Patch(':id')
    async updateProduct(
        @Param('id') id : number,
        @Body() body : {
            title: string,
            description: string,
            price: number
        }
    ) : Promise<{}> {
        const { title, description, price } = body;
        const result = await this.productsService.updateProduct(id, title, description, price);
        const updatedEntry = await this.productsService.getSpecificProduct(id);

        const response = {
            message: "Successfuly updated product information",
            data: {
                dbResult: result,
                updatedObj: updatedEntry
            }
        };

        return response;
    }

    @Delete(':id')
    async deleteProduct(
        @Param('id') id : number
    ) : Promise<{}> {
        const result = await this.productsService.deleteProduct(id);
        const updatedList = await this.productsService.getProducts();
        
        const response = {
            message: "Successfuly deleted product",
            dbResult: result,
            updatedProductList: updatedList
        }

        return response;
    }

}