import { Products } from 'src/entity/products.entity';
import { Connection } from 'typeorm';
import { PRODUCT_REPOSITORY } from 'src/core/constant';

export const productProviders = [
  {
    provide: PRODUCT_REPOSITORY,
    useFactory: (connection: Connection) => connection.getRepository(Products),
    inject: ['DATABASE_CONNECTION'],
  },
];