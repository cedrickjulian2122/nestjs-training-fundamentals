import { Inject, Injectable } from '@nestjs/common';
import { getRepository, Repository, getConnection, getManager } from 'typeorm';
import { Product } from './products.model';
import { Products } from 'src/entity/products.entity';
import { PRODUCT_REPOSITORY } from 'src/core/constant';
import { Connection } from 'typeorm';
import { Inventory } from 'src/entity/inventory.entity';
import { databaseProviders } from 'src/database/database.provider';
import { identity } from 'rxjs';

@Injectable()
export class ProductService {
    constructor( 
        @Inject(PRODUCT_REPOSITORY)
        private productsRepository: Repository<Products>,
    ) {}

    async inserProduct(prodTitle: string, desc: string, prodPrice: number, stocks: number) {
        const inventoryRepository: Repository<Inventory> = getManager().getRepository(Inventory);

        const products = new Products();
        products.title = prodTitle;
        products.description = desc;
        products.price = prodPrice;

        const productResult = await this.productsRepository.manager.save(products);

        const inventory = new Inventory();
        inventory.stocks = stocks;
        inventory.status = stocks > 50 ? 'Normal' : stocks < 10 ? 'Critical' : 'Understack';
        inventory.product_id = productResult.id;

        const inventoryResponse = await inventoryRepository.save(inventory);
        
        const response = {
            product: productResult,
            inventory: inventoryResponse
        }

        return response;
    }

    async getProducts() : Promise<Products[]> {
        const products = await this.productsRepository.find();
        return products;
    }

    async getSpecificProduct(productId: number) : Promise<Products> {
        const product = getRepository(Products)
            .createQueryBuilder("products")
            .where("products.id = :id", { id : productId})
            .getOne();

        return product;
    }

    async updateProduct(productId: number, title: string, desc: string, price: number) : Promise<{}> {
        const result = await getConnection()
            .createQueryBuilder()
            .update(Products)
            .set({
                title: title,
                description: desc,
                price: price
            })
            .where("id = :id", { id: productId })
            .execute();

        return JSON.parse(JSON.stringify(result));
    }

    async deleteProduct(productId: number) : Promise<{}> {
        const product = await getConnection()
            .createQueryBuilder()
            .delete()
            .from(Products)
            .where("id = :id", { id: productId })
            .execute();

        return product;
    }
}