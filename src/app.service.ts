import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello Programming World!';
  }

  getInteger(): JSON {
    const object = {
      id: 1, 
      data: "Testing returning of json object"
    };

    return JSON.parse(JSON.stringify(object));
  }

  getYourName(): string {
    return "My name is Cedrick!";
  }

  pushingDataToArray(): string {
    return '';
  }
}
