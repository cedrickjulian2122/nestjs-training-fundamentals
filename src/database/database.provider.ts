import { TypeOrmModule } from '@nestjs/typeorm';
import { createConnection } from 'typeorm';

export const databaseProviders = [
    {
      provide: 'DATABASE_CONNECTION',
      useFactory: async () => await createConnection({
        type: 'mysql',
        host: 'localhost',
        port: 3306,
        username: 'dummy_user',
        password: 'p@ssw0rd',
        database: 'dummy_db',
        entities: [
            __dirname + '/../**/*.entity{.ts,.js}',
        ],
        migrations: [__dirname + '/migration/*{.ts,.js}'],
        cli: {
            migrationsDir: __dirname + '/migration'
        },
      }),
    },
  ];