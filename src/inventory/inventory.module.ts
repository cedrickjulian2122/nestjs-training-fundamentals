import { Module } from '@nestjs/common';
import { InventoryController } from "./inventory.controller";
import { InventoryService } from './inventory.service';
import { DatabaseModule } from 'src/database/database.module';
import { inventoryProviders } from './inventory.providers';

@Module({
    imports: [DatabaseModule],
    controllers: [InventoryController],
    providers: [...inventoryProviders, InventoryService],
})

export class InventoryModule {}