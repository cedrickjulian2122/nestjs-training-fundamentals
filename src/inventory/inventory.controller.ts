import { Controller, Get, Param } from "@nestjs/common";
import { InventoryService } from "./inventory.service";

@Controller('inventory')
export class InventoryController {
    constructor(private readonly inventoryService: InventoryService) {}

    @Get()
    async getInventoryInfo(): Promise<{}> {
        const result = await this.inventoryService.getInventory();
        const response = {
            message: "Successfuly retrieved inventory information",
            data: result
        }
    
        return response;
    }

    @Get(':id')
    async getInventoryProductInfo(
        @Param('id') id : number
    ) : Promise<{}> {
        const inventory = await this.inventoryService.getProductInventory(id);
        const response = {
            message: "Successfuly retrieved product information about inventory",
            data: inventory
        }
        return response;
    }
}
