import { Inventory } from 'src/entity/inventory.entity';
import { Connection } from 'typeorm';
import { INVENTORY_REPOSITORY } from 'src/core/constant';

export const inventoryProviders = [
  {
    provide: INVENTORY_REPOSITORY,
    useFactory: (connection: Connection) => connection.getRepository(Inventory),
    inject: ['DATABASE_CONNECTION'],
  },
];