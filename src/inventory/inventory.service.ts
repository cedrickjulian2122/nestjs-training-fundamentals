import { Inject, Injectable } from "@nestjs/common";
import { INVENTORY_REPOSITORY } from "src/core/constant";
import { Inventory } from "src/entity/inventory.entity";
import { getConnection, getRepository, Repository } from "typeorm";

@Injectable()
export class InventoryService {
    constructor(
        @Inject(INVENTORY_REPOSITORY)
       private inventoryRepository: Repository<Inventory> 
    ) {}

    async getInventory() {
        let inventory = await this.inventoryRepository.find({ relations: ["product_id"] });
        return inventory;
    }

    async getProductInventory(productId: number) {
        const product = getRepository(Inventory)
            .createQueryBuilder("inventory")
            .where("inventory.product_id = :id", { id : productId})
            .getOne();

        return product;
    }

    async updateInventory(productId: number, quantityPurchase: number) {
        const respCompute = await this.computeStocks(productId, quantityPurchase);

        if (!respCompute.isAccepted) {
            return respCompute;
        }

        const result = await getConnection()
        .createQueryBuilder()
        .update(Inventory)
        .set({
            stocks: respCompute.data
        })
        .where("id = :id", { id: productId })
        .execute();

        return JSON.parse(JSON.stringify(result));
    }

    computeStocks = async (productId: number, purchaseQuantity: number) => {
        const res = await this.getProductInventory(productId); //Get the current number of stocks on the db
        if (res.stocks < purchaseQuantity) {
            const resp = {
                message: "Invalid purchase, current stock is low than the desired purchase quantity'",
                data: 0,
                isAccepted: false
            }
            return resp;
        }

        const resp = {
            message: '',
            isAccepted: true,
            data: res.stocks - purchaseQuantity
        }
        return resp;
    }

}